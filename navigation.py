from extras.plugins import PluginMenuButton, PluginMenuItem
from utilities.choices import ButtonColorChoices


ptidcreator_buttons = [
    PluginMenuButton(
        link='plugins:netbox_ptid_creator:ptidcreator_generate',
        title='Generate',
        icon_class='mdi mdi-plus-thick',
        color=ButtonColorChoices.GREEN
    )
]

menu_items = (
    PluginMenuItem(
        link='plugins:netbox_ptid_creator:ptidcreator',
        link_text='PTID Generator',
        buttons=ptidcreator_buttons
    ),
)