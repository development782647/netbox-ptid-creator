from setuptools import find_packages, setup

setup(
    name="netbox-ptid-creator",
    version="0.1",
    description="Netbox-Plugin for creating a unique TenantID",
    install_requires=['shortuuid==1.0.11'],
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
)
