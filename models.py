import shortuuid
from django.db import models
from netbox.models import NetBoxModel


class PTIDGenerator(NetBoxModel):
    def generate_ptid():
        random_uuid = shortuuid.ShortUUID().random(length=10).upper()
        return random_uuid

    ptid = models.CharField(default=generate_ptid, unique=True)
    without_tag = models.BooleanField(verbose_name="Without Tag", default=False)
