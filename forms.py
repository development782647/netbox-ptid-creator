from netbox.forms import NetBoxModelForm
from .models import PTIDGenerator

class PTIDGeneratorForm(NetBoxModelForm):
    class Meta:
        model = PTIDGenerator
        fields = ('ptid', 'without_tag')