# PTID Creator

Here you can find the netbox plugin **PTID Creator**, which generates a random string containing numbers and characters. This string is unique and can be used as a TenantID by the creation of a new Tenant.

The goal of this plugin is to improve the data quality in Netbox through avoiding of redundancies by tenant names.

## Installation & Integration of the plugin

First you will need to install and activate the plugin from gitlab with those comands:

```
sudo git clone https://gitlab.telemaxx.de/managedservices/netbox_ptid_creator.git
```

```
sudo -u <user> /opt/netbox/venv/bin/pip3 install git+https://gitlab.telemaxx.de/managedservices/netbox_ptid_creator.git
```

To use the plugin, you will need to replace the existing HTML-Website **object_edit.html** in your netbox application with the modified one (see folder **updated_new_templates**).

```
mv netbox_ptid_creator/updated_new_templates/object_edit.html /opt/netbox/netbox/templates/generic/object_edit.html
```

You will also need to create a new HTML-Document named **ptidcreator_form.html** for the PTID Generator mask. This new file needs to be imported in your netbox application as well.

```
mv netbox_ptid_creator/updated_new_templates/ptidcreator_form.html /opt/netbox/netbox/templates/htmx
```

Then the file **setup.py** with the setup configuration for the plugin needs to be imported as well.

```
mv netbox_ptid_creator/setup.py /opt/netbox/netbox/
```

At last, you will need to enable the plugin in **/opt/netbox/netbox/netbox/configuration.py**:

```python
PLUGINS = [
    'netbox_ptid_creator'
]
```

### Migrate new plugin

You need to use the following commands, so that you can migrate the plugin in your netbox application:

```
sudo python3 manage.py makemigrations netbox_ptid_creator
```

```
sudo python3 manage.py migrate
```

And then you will need to restart netbox and its rqworker:

```
sudo systemctl restart netbox netbox-rq
```

### Automation by updating and deleting tenants

You will need to import the JSON file **netbox_webhooks.json** from the folder **./webhooks** into your netbox application, so that the description field from your tag gets updated after creating the new tenant (Description from tenant = description from tag) through the Webhook **Put Tenant Description to Tag Description**. <br>
With the second webhook named **Delete Tag after Tenant Deletion**, the created tag gets deleted after the associated tenant gets deleted.

You can import the webhooks through the function **Import** as upload file type JSON.

## Using the plugin

After the plus-button next to the plugin **PTID Generator** is clicked in Netbox in the window **Plugins**, a random string will be created. After clicking the button **Use for new Tenant**, a new window for creating a tenant will open and this generated PTID will be put into the field **Name**.
Before the creation-window gets opened, a new **Tag** gets created in the background with the same name as the Tenant, when the checkbox **Without Tag** isn't checked.
