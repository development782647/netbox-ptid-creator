from django.urls import path
from netbox.views import HomeView
from . import views

urlpatterns = (
    # PTID Generator
    path('ptid-creator/', HomeView.as_view(), name='ptidcreator'),
    path('ptid-creator/generate/', views.PTIDGeneratorEditView.as_view(), name='ptidcreator_generate'),
)