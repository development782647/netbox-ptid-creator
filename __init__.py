from extras.plugins import PluginConfig

class NetBoxPTIDCreatorConfig(PluginConfig):
    name = 'netbox_ptid_creator'
    verbose_name = ' NetBox PTID Creator'
    description = 'Creating unique PTIDs for tenants'
    version = '0.1'
    base_url = 'ptid-creator'
    
    
config = NetBoxPTIDCreatorConfig