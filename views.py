from netbox.views import generic
from . import forms, models


class PTIDGeneratorEditView(generic.ObjectEditView):
    queryset = models.PTIDGenerator.objects.all()
    form = forms.PTIDGeneratorForm